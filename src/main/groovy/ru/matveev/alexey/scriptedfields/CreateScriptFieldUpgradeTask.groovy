package ru.matveev.alexey.scriptedfields

import com.atlassian.jira.issue.context.GlobalIssueContext
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService
import com.atlassian.sal.api.message.Message
import com.atlassian.sal.api.upgrade.PluginUpgradeTask
import com.onresolve.scriptrunner.runner.ScriptRunnerImpl
import com.onresolve.scriptrunner.test.ScriptFieldCreationInfo
import groovy.util.logging.Log4j

import javax.inject.Named

@Log4j
@Named
@ExportAsService
class CreateScriptFieldUpgradeTask extends AbstractUpgradeTask implements PluginUpgradeTask {


    @Override
    int getBuildNumber() {
         return 1
    }

    @Override
    String getShortDescription() {
         return "This upgrade task creates a scripted field"
    }

    @Override
    Collection<Message> doUpgrade() throws Exception {
         def scriptFieldCreation = ScriptFieldCreationInfo.Builder.newBuilder()
                .setName("TestScriptFieldSimpleNumberX")
                .setSearcherKey(ScriptRunnerImpl.PLUGIN_KEY + ":exactnumber")
                .setTemplate("float")
                .setContexts([GlobalIssueContext.instance])
                .setScriptFile("ru/matveev/alexey/main/scriptedfields/scriptedfield.groovy")
                .build()
        scriptFieldCreation.create()
        return null
    }

}
