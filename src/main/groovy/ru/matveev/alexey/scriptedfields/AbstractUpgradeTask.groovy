package ru.matveev.alexey.scriptedfields

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil
import groovy.util.logging.Log4j;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

@Log4j
abstract class AbstractUpgradeTask {
    public String getPluginKey() {
        Bundle bundle = FrameworkUtil.getBundle(AbstractUpgradeTask.class);
        return OsgiHeaderUtil.getPluginKey(bundle);
    }
}
